//
//  main.cpp
//  random_BT
//
//  Created by chenhsi on 2016/11/7.
//  Copyright © 2016年 chenhsi. All rights reserved.
//

#include <iostream>
using namespace std;


struct Node
{
    int data;
    Node* left;
    Node* right;
    
};

struct match{
    int data;
    int level;

};

Node* previous;
Node* Root = new Node;
void Insert(Node* root, int data){

    if(root==NULL){
        Node* temp = new Node;
        temp->data = data;
        temp->left = NULL;
        temp->right = NULL;
        
        if(previous->data > temp->data)
          previous->left = temp;
        else if (previous->data < temp->data)
          previous->right = temp;
    }
    
    else if (root->data > data){
      previous = root;
      Insert(root->left, data);

    }
    
    else if (root->data < data){
      previous = root;
      Insert(root->right, data);
    
    }
    
    
}

void PrintTree(Node* root){

}
int FindLevel(Node* root,int data, int level){
    if(root == NULL)
        return 0;
    if(root->data == data)
        return level;
    else {
        int left_search = FindLevel(root->left, data, level+1);
        int right_search = FindLevel(root->right, data, level+1);
        if(left_search ==0)
            return right_search;
        else
            return left_search;
    
    }
    
}


int main()
{
    // put the root data = 10
    Root->data = 10;
    Root->left = NULL;
    Root->right = NULL;
    int InputValue[3] = {8,12,6};
    int n;
    match arr[3];
    
    for(int i=0;i<3;i++){
        Insert(Root, InputValue[i]);
    }
    
    for(int i=0;i<3;i++){
     int ans = FindLevel(Root, InputValue[i], 0);
        arr[i].level = ans;
        arr[i].data = InputValue[i];
    }
    
    cout<<"input a value you want to check"<<endl;
    cin>>n;
    int ans = FindLevel(Root, n, 0);
    
    for(int i=0;i<3;i++){
        if(ans==arr[i].level){
            cout<<"the element "<<arr[i].data<<" is in level "<<arr[i].level<<endl;
        }
    }
    
    return 0;
}

